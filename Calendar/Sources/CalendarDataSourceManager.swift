import Foundation

class CalendarDataSourceManager {
    fileprivate let startDate: Date
    fileprivate let calendar = Foundation.Calendar.current
    fileprivate let dateManager: DateManager = DateManager()
    fileprivate lazy var calendarStartsOnMonday: Bool = { [unowned self] in
        return self.calendar.firstWeekday == 2
    }()


    fileprivate func calendarDateComponents(_ date: Date) -> DateComponents {
        return (calendar as NSCalendar).components([.day, .month, .year], from: date)
    }
   

    init(startDate: Date) {
        self.startDate = startDate
    }

    func numberOfMonths() -> Int {
        let years = Date().yearsFrom(startDate) + 1
        return 12 * years
    }

    func currentDateIndex() -> IndexPath {
        let currentMonth  = self.numberOfMonths() - CalendarDate.fromDate(Date()).month
        let todayComponenets = (calendar as NSCalendar).components(.day, from: Date())
        let startIndexAdd = (0...15)
            .map { self.calendarDateForMonth(currentMonth - 1, dayIndex: $0) }
            .filter { $0.isFromAnotherMonth }.count
        return IndexPath(item: startIndexAdd + todayComponenets.day! - 1, section: currentMonth - 1)
    }

    func indexForDate(_ date: CalendarDate) -> IndexPath {
        let calendarStartDate = CalendarDate.fromDate(startDate)
        let month = abs(12 * (date.year - calendarStartDate.year)) + date.month
        let startIndexAdd = (0...15)
            .map { self.calendarDateForMonth(month, dayIndex: $0) }
            .filter { $0.isFromAnotherMonth }.count
        return IndexPath(item: startIndexAdd + date.day - 1 - 6, section: month - 1)
    }

    func numberOfWeeksInMonth(_ month: Int) -> Int {
        let date = dateForFirstDayInMonth(month)
        return dateManager.numberOfWeeksInMonth(date)
    }

    func calendarDateForMonth(_ monthIndex: Int, dayIndex: Int) -> CalendarDate {
        let firstDayInMonth = dateForFirstDayInMonth(monthIndex)
        let monthWeekdayStart = (self.calendar as NSCalendar).components([.weekday], from: firstDayInMonth.date as Date).weekday
        var monthWeekdayStartAdjusted: Int = monthWeekdayStart! - 1
        switch(calendarStartsOnMonday, monthWeekdayStart) {
            case(true, 1?): monthWeekdayStartAdjusted = monthWeekdayStart! + 5
            case(true, _): monthWeekdayStartAdjusted = monthWeekdayStart! - 2
            case _: monthWeekdayStartAdjusted = monthWeekdayStart! - 1
        }
        var indexDateComponents = DateComponents()
        indexDateComponents.day = dayIndex - monthWeekdayStartAdjusted
        let indexDate = (calendar as NSCalendar).date(byAdding: indexDateComponents, to: firstDayInMonth.date as Date, options: [])!
        let components = (calendar as NSCalendar).components([.year, .month], from: indexDate)
        let isFromThisMonth = firstDayInMonth.month == components.month && firstDayInMonth.year == components.year
        let calendarDate = CalendarDate.fromDate(indexDate, isFromAnotherMonth: !isFromThisMonth)
        return calendarDate
    }

    fileprivate func dateForFirstDayInMonth(_ month: Int) -> CalendarDate {
        var components = DateComponents()
        components.month = month
        let date  = (calendar as NSCalendar).date(byAdding: components, to: self.startDate, options: [])!
        var firstDateComponenets = (calendar as NSCalendar).components([.year, .month, .day], from: date)
        firstDateComponenets.day = 1
        let firstDate = calendar.date(from: firstDateComponenets)!
        return  CalendarDate.fromDate(firstDate)
    }
}
