//
//  Calendar.swift
//  Calendar
//
//  Created by Jon Andersen on 1/19/16.
//  Copyright © 2016 Andersen. All rights reserved.
//

import Foundation

public protocol CalendarViewDelegate : class {
    func calendarBuildCell(_ cell: CalendarDateCell, calendarDate: CalendarDate)
    func calendarDidSelectDayCell(_ cell: CalendarDateCell, calendarDate: CalendarDate)
    func calendarDidSelectMonthCell(_ cell: CalendarMonthCell, calendarDate: CalendarDate)
    func calendarDateChanged(_ calendarDate: CalendarDate)
}

enum CalendarMode {
    case Month
    case Day
}

public class CalendarView: UIView {
    open let calendarCollectionView: UICollectionView!
    private let calendarMonthCollectionView: UICollectionView!
    private var currentMode = CalendarMode.Day

    private  var calendarDayDataSource: CalendarDayDataSource!
    private  var calendarMonthDataSource: CalendarMonthDataSource!
    private  var calendarDayDelegate: CalendarCollectionViewDelegate!
    private  var calendarMonthDelegate: CalendarCollectionViewDelegate!


    public weak var delegate: CalendarViewDelegate? {
        didSet {
            calendarDayDataSource.delegate = delegate
            calendarDayDelegate.delegate = delegate
            calendarMonthDelegate.delegate = delegate
        }
    }

    open func reloadData() {
        calendarCollectionView.reloadData()
    }

    public required override init(frame: CGRect) {
        calendarCollectionView = UICollectionView(frame: frame,
            collectionViewLayout:  CalendarLayout())
        calendarMonthCollectionView = UICollectionView(frame: frame,
                                                  collectionViewLayout:  CalendarLayout())
        super.init(frame: frame)
    }

    required public init?(coder aDecoder: NSCoder) {
        calendarCollectionView = UICollectionView(frame: .zero,
            collectionViewLayout:  CalendarLayout())
        calendarMonthCollectionView = UICollectionView(frame: .zero,
                                                       collectionViewLayout:  CalendarLayout())

        super.init(coder: aDecoder)
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        self.calendarCollectionView.frame = self.frame
        self.calendarMonthCollectionView.frame = self.frame

    }

    open func showYearView(_ calendarDate: CalendarDate?) {
        if currentMode == .Month {
            return
        }
        currentMode = .Month
        self.calendarMonthCollectionView.isHidden = false
        scrollToDate(calendarDate)
        UIView.animate(withDuration: 0.33, animations: {
            //self.calendarCollectionView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001);
            self.calendarCollectionView.alpha = 0.0
            self.calendarMonthCollectionView.alpha = 1.0
        }, completion: { _ in
            self.calendarCollectionView.isHidden = true
        })
    }

    open func showMonthView(_ calendarDate: CalendarDate?) {
        if currentMode == .Day {
            return
        }
        currentMode = .Day
        scrollToDate(calendarDate)
        self.calendarCollectionView.isHidden = false
        //calendarCollectionView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001);
        UIView.animate(withDuration: 0.3, animations: {
            self.calendarCollectionView.alpha = 1.0
            self.calendarMonthCollectionView.alpha = 0.0
            //self.calendarCollectionView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0);
        }, completion: { _ in
            self.calendarMonthCollectionView.isHidden = true
        })
    }

    open override func awakeFromNib() {
        super.awakeFromNib()
        self.calendarMonthCollectionView.alpha = 0.0
        self.calendarMonthCollectionView.isHidden = true
        self.addSubview(calendarCollectionView)
        self.addSubview(calendarMonthCollectionView)
        calendarCollectionView.contentInset = UIEdgeInsets.zero

        calendarCollectionView.backgroundColor = self.backgroundColor
        calendarMonthCollectionView.backgroundColor = self.backgroundColor

        calendarCollectionView.showsHorizontalScrollIndicator = false
        calendarCollectionView.showsVerticalScrollIndicator = false
        let startDate = CalendarDate(year: 2000, month: 1, day: 1).date
        let calendarDataSource = CalendarDataSourceManager(startDate: startDate)
        let calendarFrameworkBundle = Bundle(for: CalendarView.self)
        let calendarResourceBundle: Bundle
        if let bundlePath = calendarFrameworkBundle.path(forResource: "Calendar", ofType: "bundle") {
            calendarResourceBundle = Bundle(path: bundlePath) ?? calendarFrameworkBundle
        } else {
            calendarResourceBundle = calendarFrameworkBundle
        }
        self.configureDayCollectionView(calendarResourceBundle)
        self.configuredMonthCollectionView(calendarResourceBundle)

        calendarDayDataSource = CalendarDayDataSource(calendarDataSource: calendarDataSource)
        calendarMonthDataSource = CalendarMonthDataSource(startDate: startDate)
        calendarDayDelegate = CalendarCollectionViewDelegate(collectionView: calendarCollectionView, itemsPerRow: 7)
        calendarMonthDelegate = CalendarCollectionViewDelegate(collectionView: calendarMonthCollectionView, itemsPerRow: 4)
        self.calendarCollectionView.dataSource = calendarDayDataSource
        self.calendarCollectionView.delegate = calendarDayDelegate
        self.calendarMonthCollectionView.dataSource = calendarMonthDataSource
        self.calendarMonthCollectionView.delegate = calendarMonthDelegate
    }

    open func scrollToDate(_ calendarDate: CalendarDate? = nil) {
        let calendarDate = calendarDate ?? CalendarDate.fromDate(Date())
        self.delegate?.calendarDateChanged(calendarDate)
        switch currentMode {
        case .Day:
            let index = self.calendarDayDataSource.indexForDate(calendarDate)
            calendarCollectionView.scrollToItem(at: index, at: .centeredVertically, animated: false)
        case .Month:
            let index = self.calendarMonthDataSource.indexForDate(calendarDate)
            calendarMonthCollectionView.scrollToItem(at: index, at: .centeredVertically, animated: false)
        }
    }



    fileprivate func configureDayCollectionView(_ bundle: Bundle) {
        let cellNib = UINib(nibName: "CalendarDateCell", bundle: bundle)
        self.calendarCollectionView.register(cellNib,
                                                forCellWithReuseIdentifier: CalendarDateCell.identifier)
        let dayHeaderNib = UINib(nibName: CalendarDayHeader.reuseIdentifier, bundle: bundle)
        self.calendarCollectionView.register(dayHeaderNib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                                withReuseIdentifier: CalendarDayHeader.reuseIdentifier)
        if let layout = self.calendarCollectionView.collectionViewLayout as? CalendarLayout {
            layout.headerReferenceSize = CGSize(width: self.frame.width,
                                                height: CalendarDayHeader.height)
        }
    }

    fileprivate func configuredMonthCollectionView(_ bundle: Bundle) {
        let monthHeaderNib = UINib(nibName: CalendarMonthHeader.reuseIdentifier, bundle: bundle)
        if let layout = self.calendarMonthCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.headerReferenceSize = CGSize(width: self.frame.width,
                                                height: CalendarMonthHeader.height)
        }
        self.calendarMonthCollectionView.register(monthHeaderNib,
                                                     forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                                     withReuseIdentifier: CalendarMonthHeader.reuseIdentifier)
        let calendarMonthCell = UINib(nibName: "CalendarMonthCell", bundle: bundle)
        self.calendarMonthCollectionView.register(calendarMonthCell,
                                                     forCellWithReuseIdentifier: CalendarMonthCell.reuseIdentifier)
    }
}
