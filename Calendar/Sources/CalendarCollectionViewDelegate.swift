//
//  CalendarCollectionViewDelegate.swift
//  Calendar
//
//  Created by Jon Andersen on 4/11/16.
//  Copyright © 2016 Andersen. All rights reserved.
//

import UIKit

class CalendarCollectionViewDelegate: NSObject, UICollectionViewDelegate {
    fileprivate let itemsPerRow: CGFloat
    fileprivate weak var collectionView: UICollectionView?
    weak var delegate: CalendarViewDelegate?

    init(collectionView: UICollectionView, itemsPerRow: Int) {
        self.itemsPerRow = CGFloat(itemsPerRow)
        self.collectionView = collectionView
        super.init()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? CalendarDateCell {
            delegate?.calendarDidSelectDayCell(cell, calendarDate: cell.calendarDate)
        } else if let cell = collectionView.cellForItem(at: indexPath) as? CalendarMonthCell {
            delegate?.calendarDidSelectMonthCell(cell, calendarDate: cell.calendarDate)
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let isLandscape = UIDevice.current.orientation.isLandscape
        if UIDevice.current.userInterfaceIdiom == .pad {
            let size: CGFloat
            if isLandscape {
                size = collectionView.frame.width / (itemsPerRow + 1)
            } else {
                size = collectionView.frame.width / (itemsPerRow)
            }
            return CGSize(width: size, height: size)
        } else {
            let size = collectionView.frame.width / itemsPerRow
            return CGSize(width: size - 8, height: size - 8)
        }
    }

    private var startScroll: CGFloat = 0.0
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        startScroll = scrollView.contentOffset.y
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        startScroll = 0.0
        scrollPositionChanged()
    }

    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        scrollPositionChanged()
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard abs(Double(scrollView.contentOffset.y - startScroll)) > 10.0 else {
            return
        }
        startScroll = scrollView.contentOffset.y
    }

    fileprivate func scrollPositionChanged() {
        if let collectionView = self.collectionView {
            var visibleRect = CGRect()
            visibleRect.origin = collectionView.contentOffset
            visibleRect.size = collectionView.bounds.size
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.maxY)
            let indexPath: IndexPath
            if let ip = collectionView.indexPathForItem(at: visiblePoint) {
                indexPath = ip
            } else {
                let cells = collectionView.visibleCells
                indexPath = collectionView.indexPath(for: cells[0] )! as IndexPath
            }
            if let cell = collectionView.cellForItem(at: indexPath) as? CalendarDateCell {
                delegate?.calendarDateChanged(cell.calendarDate)
            } else if let cell = collectionView.cellForItem(at: indexPath) as? CalendarMonthCell {
                delegate?.calendarDateChanged(cell.calendarDate)
            }
        }
    }


    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 4
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 4
    }
}
