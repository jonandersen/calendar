//
//  CalendarManager.swift
//  Calendar
//
//  Created by Jon Andersen on 1/16/16.
//  Copyright © 2016 Andersen. All rights reserved.
//

import Foundation

class CalendarDayDataSource: NSObject, UICollectionViewDataSource {
    private let calendarDataSource: CalendarDataSourceManager
    private var loadingMore = false
    private let numberOfItemsPerRow: CGFloat = 7
    private let dateFormatter = DateFormatter()

    weak var delegate: CalendarViewDelegate?

    required init(calendarDataSource: CalendarDataSourceManager) {
        self.calendarDataSource = calendarDataSource
        super.init()
        dateFormatter.dateFormat = "EEE, MM d"
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String,
        at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(
            ofKind: kind,
            withReuseIdentifier: CalendarDayHeader.reuseIdentifier,
            for: indexPath) as! CalendarDayHeader
        let calendarDate = calendarDataSource.calendarDateForMonth((indexPath as NSIndexPath).section, dayIndex: 12)
        let hidden = (0...6)
            .map { self.calendarDataSource.calendarDateForMonth((indexPath as NSIndexPath).section, dayIndex: $0) }
            .filter { $0.isFromAnotherMonth }
        let padding: CGFloat = CGFloat(hidden.count) * ((collectionView.frame.width / numberOfItemsPerRow) + 4)
        header.monthLabel.text = "\(dateFormatter.shortMonthSymbols[calendarDate.month - 1])"
        header.leadingConstraint.constant = max(padding, 8.0)
        if calendarDate.isThisMonth() {
            header.monthLabel.textColor = Colors.headerCurrentTextColor
        } else {
            header.monthLabel.textColor = Colors.headerTextColor
        }
        return header
    }

    func indexForDate(_ calendarDate: CalendarDate) -> IndexPath {
        return calendarDataSource.indexForDate(calendarDate)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return calendarDataSource.numberOfMonths()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7 * calendarDataSource.numberOfWeeksInMonth(section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CalendarDateCell.identifier, for: indexPath) as! CalendarDateCell
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        let calendarDate = calendarDataSource.calendarDateForMonth((indexPath as NSIndexPath).section, dayIndex: (indexPath as NSIndexPath).item)
        cell.textLabel.text = "\(calendarDate.day)"
        if calendarDate.isFromAnotherMonth {
            cell.isHidden = true
        } else {
            cell.isHidden = false
        }
        cell.calendarDate = calendarDate
        delegate?.calendarBuildCell(cell, calendarDate: calendarDate)
        cell.accessibilityIdentifier = calendarDate.identifier()
        CATransaction.commit()
        return cell
    }
}
