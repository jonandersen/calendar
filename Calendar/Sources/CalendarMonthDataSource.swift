//
//  CalendarMonthDatasource.swift
//  Calendar
//
//  Created by Jon Andersen on 4/10/16.
//  Copyright © 2016 Andersen. All rights reserved.
//

import Foundation


class CalendarMonthDataSource: NSObject, UICollectionViewDataSource {
    fileprivate let startDate: Date
    fileprivate let dateFormatter = DateFormatter()

    init(startDate: Date) {
        self.startDate = startDate
        super.init()
        dateFormatter.dateFormat = "EEE, MM d"
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(
            ofKind: kind,
            withReuseIdentifier: CalendarMonthHeader.reuseIdentifier,
            for: indexPath) as! CalendarMonthHeader
        let calendarDate = calendarDateForIndexPath(indexPath)
        header.yearLabel.text = "\(calendarDate.year)"
        if calendarDate.isThisYear() {
            header.yearLabel.textColor = Colors.headerCurrentTextColor
        } else {
            header.yearLabel.textColor = Colors.headerTextColor
        }
        return header
    }

    func indexForDate(_ calendarDate: CalendarDate) -> IndexPath {
        let year = abs(calendarDate.date.yearsFrom(startDate))
        return IndexPath(item: 6, section: year)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let months = Date().monthsFrom(startDate)
        return Int(ceil(Double(months) / 12))
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CalendarMonthCell.reuseIdentifier, for: indexPath) as! CalendarMonthCell
        let calendarDate = calendarDateForIndexPath(indexPath)
        cell.label.text = "\(dateFormatter.shortMonthSymbols[calendarDate.month - 1])"
        cell.calendarDate = calendarDate
        return cell
    }

    fileprivate func calendarDateForIndexPath(_ indexPath: IndexPath) -> CalendarDate {
        let month = (indexPath as NSIndexPath).item
        let year = (indexPath as NSIndexPath).section
        var components = DateComponents()
        components.year = year
        components.month = month
        let date = (Foundation.Calendar.current as NSCalendar).date(byAdding: components, to: self.startDate, options: [])!
        return CalendarDate.fromDate(date)
    }
}
