//
//  CalendarDateManager.swift
//  leapsecond
//
//  Created by Jon Andersen on 1/15/16.
//  Copyright © 2016 Andersen. All rights reserved.
//

import Foundation


class DateManager {
    fileprivate let calendar =  Foundation.Calendar.current
    fileprivate let firstDayOfWeek = 1

    func numberOfWeeksInMonth(_ calendarDate: CalendarDate) -> Int {
        let calendar = Foundation.Calendar.current
        let weekRange = (calendar as NSCalendar).range(of: .weekOfYear, in: .month, for: calendarDate.date as Date)
        return  weekRange.length
    }
}

extension Date {
    func yearsFrom(_ date: Date) -> Int {
        return (Foundation.Calendar.current as NSCalendar).components(.year, from: date, to: self, options: []).year!
    }
    func monthsFrom(_ date: Date) -> Int {
        return (Foundation.Calendar.current as NSCalendar).components(.month, from: date, to: self, options: []).month!
    }
    func weeksFrom(_ date: Date) -> Int {
        return (Foundation.Calendar.current as NSCalendar).components(.weekOfYear, from: date, to: self, options: []).weekOfYear!
    }
    func daysFrom(_ date: Date) -> Int {
        return (Foundation.Calendar.current as NSCalendar).components(.day, from: date, to: self, options: []).day!
    }
}
