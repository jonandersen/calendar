//
//  NoAnimationLayout.swift
//  leapsecond
//
//  Created by Jon Andersen on 1/15/16.
//  Copyright © 2016 Andersen. All rights reserved.
//

import Foundation


class CalendarLayout: UICollectionViewFlowLayout {
    var isInsertingCellsToTop: Bool = false
    var contentSizeWhenInsertingToTop: CGSize?

    override func prepare() {
        super.prepare()
        if isInsertingCellsToTop == true {
            if let collectionView = collectionView, let oldContentSize = contentSizeWhenInsertingToTop {
                let newContentSize = collectionViewContentSize
                let contentOffsetY = collectionView.contentOffset.y + (newContentSize.height - oldContentSize.height)
                let newOffset = CGPoint(x: collectionView.contentOffset.x, y: contentOffsetY)
                collectionView.contentOffset = newOffset
            }
            contentSizeWhenInsertingToTop = nil
            isInsertingCellsToTop = false
        }
    }

    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return nil
    }

    override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return nil
    }

    override func initialLayoutAttributesForAppearingSupplementaryElement(ofKind elementKind: String,
        at elementIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return nil
    }

    override func finalLayoutAttributesForDisappearingSupplementaryElement(ofKind elementKind: String,
        at elementIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return nil
    }

    override func initialLayoutAttributesForAppearingDecorationElement(ofKind elementKind: String,
        at decorationIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return nil
    }

    override func finalLayoutAttributesForDisappearingDecorationElement(ofKind elementKind: String,
        at decorationIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return nil
    }
}
