//
//  CalendarDate.swift
//  Calendar
//
//  Created by Jon Andersen on 1/10/16.
//  Copyright © 2016 Andersen. All rights reserved.
//

import Foundation

public struct CalendarDate {
    public let year: Int
    public let month: Int
    public let week: Int
    public let day: Int
    public var components = DateComponents()
    public let isFromAnotherMonth: Bool
    public let date: Date

    fileprivate let description: String
    fileprivate let calendar = Foundation.Calendar.current

    public static func empty() -> CalendarDate {
        return CalendarDate(year: 0, month: 0, week: 0, day: 0, isFromAnotherMonth: false)
    }

    public init(year: Int, month: Int, week: Int = 0, day: Int, isFromAnotherMonth: Bool = false) {
        self.year = year
        self.month = month
        self.week = week
        self.day = day
        self.isFromAnotherMonth = isFromAnotherMonth
        self.description = "\(year)-\(month)-\(day)"
        components.year = year
        components.month = month
        components.day = day
        self.date = Foundation.Calendar.current.date(from: components)!
    }

    public static func fromDate(_ date: Date, isFromAnotherMonth: Bool = false) -> CalendarDate {
        let calendar = Foundation.Calendar.current
        let components = (calendar as NSCalendar).components([.year, .month, .weekOfYear, .day, .second], from: date)
        return CalendarDate(
            year: components.year!,
            month: components.month!,
            week: components.weekOfYear!,
            day: components.day!,
            isFromAnotherMonth: isFromAnotherMonth)
    }

    public func identifier() -> String {
        return description
    }

    public func isSameDay(_ date: CalendarDate) -> Bool {
        return isSameMonth(date) && date.day == day
    }

    public func isSameMonth(_ date: CalendarDate) -> Bool {
        return isSameYear(date) && date.month == self.month
    }

    public func isSameYear(_ date: CalendarDate) -> Bool {
        return date.year == self.year
    }

    fileprivate func todayComponents() -> DateComponents {
        return (calendar as NSCalendar).components([.year, .month, .weekOfYear, .day], from: Date())
    }

    public func isThisYear() -> Bool {
        return todayComponents().year == self.year
    }

    public func isThisMonth() -> Bool {
        return todayComponents().month == self.month && isThisYear()
    }

    public func isThisWeek() -> Bool {
        return todayComponents().weekOfYear == self.week && isThisMonth()
    }

    public func isToday() -> Bool {
        return todayComponents().day == self.day && isThisWeek()
    }
}

extension CalendarDate : Equatable {}
public func == (lhs: CalendarDate, rhs: CalendarDate) -> Bool {
    return lhs.year == rhs.year && lhs.month == rhs.month && lhs.day == rhs.day
}
