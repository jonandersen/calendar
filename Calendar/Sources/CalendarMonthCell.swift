import Foundation
import UIKit

open class CalendarMonthCell: UICollectionViewCell {
    static let reuseIdentifier: String = "CalendarMonthCell"
    var calendarDate: CalendarDate = CalendarDate.empty()
    @IBOutlet weak var label: UILabel!
    fileprivate let circleRatio: CGFloat = 1.0

    open override func awakeFromNib() {
    }
}
