//
//  ViewController.swift
//  Example
//
//  Created by Jon Andersen on 4/10/16.
//  Copyright © 2016 Andersen. All rights reserved.
//

import UIKit
import Calendar

class ViewController: UIViewController, CalendarViewDelegate {

    @IBOutlet weak var calendarView: CalendarView!
    @IBOutlet weak var minimize: UIBarButtonItem!
    @IBOutlet weak var expandButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        calendarView.delegate = self

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        calendarView.scrollToDate()
    }

    @IBAction func minimizePressed(_ sender: UIBarButtonItem) {
        calendarView.showYearView(currentCalendarDate)
    }
    @IBAction func expandPressed(_ sender: UIBarButtonItem) {
        
    }

    func calendarBuildCell(_ cell: CalendarDateCell, calendarDate: CalendarDate) {

    }
    
    fileprivate var currentCalendarDate: CalendarDate?
    func calendarDateChanged(_ calendarDate: CalendarDate) {
        self.title = "\(calendarDate.year)"
        self.currentCalendarDate = calendarDate
    }


    func calendarDidSelectDayCell(_ cell: CalendarDateCell, calendarDate: CalendarDate) {
        NSLog("Selected Day: \(calendarDate.day)")

    }

    func calendarDidSelectMonthCell(_ cell: CalendarMonthCell, calendarDate: CalendarDate) {
        NSLog("Selected Month: \(calendarDate.month)")
        calendarView.showMonthView(calendarDate)
    }


}
